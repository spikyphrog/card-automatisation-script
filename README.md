This project is a side project that was intended to help me speed up
the creation of cards, by setting a template and populating the text on the cards
based on a JSON file with all their attributes and exporting them out for printing.

This script is for Adobe Illustrator.

Hristo Stoyanov 2020